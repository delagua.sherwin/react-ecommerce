import React, { useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import NavBar from './component/NavBar';
import Home from './pages/Home';
import AddProduct from './pages/AddProduct';
import Cart from './pages/Cart';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Register from './pages/Register';
import Shop from './pages/Shop';
import UserContext from './UserContext';
import SingleProduct from './pages/SingleProduct';

function App() {
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true',
    _id: localStorage.getItem('_id'),
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      accessToken: null,
      isAdmin: null,
      _id: null,
    });
  };

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <Router>
        <NavBar />
        <Container>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/shop' component={Shop} />
            <Route path='/product/:productId' component={SingleProduct} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/register' component={Register} />
            <Route exact path='/add-product' component={AddProduct} />
            <Route exact path='/cart' component={Cart} />
            <Route component={NotFound} />
          </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
  );
}

export default App;
