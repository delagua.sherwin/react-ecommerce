import React, { Fragment, useContext } from 'react';
import logo from '../logo.svg';
import Navbar from 'react-bootstrap/Navbar';
import { Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function NavBar() {
  const history = useHistory();
  const { user, setUser, unsetUser } = useContext(UserContext);
  console.log(user);

  const logout = () => {
    unsetUser();
    setUser({ accessToken: null });
    history.push('/login');
  };

  let rightNav =
    user.accessToken !== null ? (
      user.isAdmin ? (
        <Fragment>
          <Nav.Link as={NavLink} to='/add-product'>
            Add Product
          </Nav.Link>
          <Nav.Link onClick={logout}>Logout</Nav.Link>
        </Fragment>
      ) : (
        <Fragment>
          <Nav.Link as={NavLink} to='/cart'>
            Cart
          </Nav.Link>
          <Nav.Link onClick={logout}>Logout</Nav.Link>
        </Fragment>
      )
    ) : (
      <Fragment>
        <Nav.Link as={NavLink} to='/login'>
          Login
        </Nav.Link>
        <Nav.Link as={NavLink} to='/register'>
          Register
        </Nav.Link>
      </Fragment>
    );

  return (
    <Navbar bg='light' expand='lg'>
      <Navbar.Brand as={Link} to='/'>
        <img
          alt=''
          src={logo}
          width='150'
          height='60'
          className='d-inline-block align-top'
        />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className='mr-auto'>
          <Nav.Link as={NavLink} to='/'>
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to='/shop'>
            Shop
          </Nav.Link>
        </Nav>
        <Nav className='ml-auto'>{rightNav}</Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
