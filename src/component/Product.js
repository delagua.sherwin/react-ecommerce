import React from 'react';
import { Card, Button } from 'react-bootstrap';

export default function Product({ product }) {
  const { image, name, brand, description, price, _id } = product;
  return (
    <div className='container'>
      <div>
        <Card
          bg='light'
          text='dark'
          style={{
            width: '20rem',
            height: '100%',
            margin: '5px 5px',
            padding: '0px',
          }}
        >
          <Card.Img
            variant='top'
            src={'https://sheltered-badlands-71188.herokuapp.com/' + image}
          />

          <Card.Body>
            <Card.Title as='h3' class='text-uppercase'>
              {name}
            </Card.Title>
            <Card.Text>
              <h4>Brand:</h4>
              <p>{brand}</p>
              <p>{description}</p>
              <h4>Price:</h4>
              <p>Php {price}</p>
              <Card.Link href={`/product/${_id}`}>Details</Card.Link>
            </Card.Text>
          </Card.Body>
          <Card.Footer></Card.Footer>
        </Card>
      </div>
    </div>
  );
}
