import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

export default function AddProduct() {
  const history = useHistory();
  const { user } = useContext(UserContext);

  const [image, setImage] = useState('');
  const [name, setName] = useState('');
  const [brand, setBrand] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [countInStock, setCountInStock] = useState(0);

  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    if (
      image !== '' &&
      name !== '' &&
      brand !== '' &&
      description !== '' &&
      price !== 0 &&
      countInStock !== 0
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [image, name, brand, description, price, countInStock]);

  function AddProduct(e) {
    e.preventDefault();

    fetch(
      'https://sheltered-badlands-71188.herokuapp.com/api/v1/product/addproduct',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
        body: JSON.stringify({
          image: image,
          name: name,
          brand: brand,
          description: description,
          price: price,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // if (data === true) {
        //   Swal.fire({
        //     title: 'Product Created',
        //     icon: 'success',
        //   });
        //   history.push('/shop');
        // } else {
        //   Swal.fire({
        //     title: 'Product Creation Failed',
        //     icon: 'error',
        //   });
        // }
      });

    setImage('');
    setName('');
    setBrand('');
    setDescription('');
    setPrice(0);
    setCountInStock(0);
  }

  return (
    <>
      <h1>Create Course</h1>
      <Form enctype='multipart/form-data' onSubmit={(e) => AddProduct(e)}>
        <Form.Group>
          <Form.File
            id='exampleFormControlFile1'
            label='Upload Image Type: jpg/png Size: 5mb'
            type='file'
            onChange={(e) => setImage(e.target.file)}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Name:</Form.Label>
          <Form.Control
            type='text'
            placeholder='Enter Name of the Product'
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Brand:</Form.Label>
          <Form.Control
            type='text'
            placeholder='Enter Name of the Product'
            value={brand}
            onChange={(e) => setBrand(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Description:</Form.Label>
          <Form.Control
            type='text'
            placeholder='Enter Description'
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Price:</Form.Label>
          <Form.Control
            type='number'
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Stock Count:</Form.Label>
          <Form.Control
            type='number'
            value={countInStock}
            onChange={(e) => setCountInStock(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button type='submit' variant='primary'>
            Submit
          </Button>
        ) : (
          <Button type='submit' variant='primary' disabled>
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}
