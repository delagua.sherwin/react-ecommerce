import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useHistory } from 'react-router-dom';

export default function Login() {
  const history = useHistory();
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [loginButton, setLoginButton] = useState(false);

  useEffect(() => {
    if (email !== '' && password !== '') {
      setLoginButton(true);
    } else {
      setLoginButton(false);
    }
  }, [email, password]);

  function login(e) {
    e.preventDefault();
    fetch('https://sheltered-badlands-71188.herokuapp.com/api/v1/user/signin', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data.token);
        if (data.token !== undefined) {
          localStorage.setItem('accessToken', data.token);
          setUser({ accessToken: data.token });

          Swal.fire({
            title: 'Yaaaaaay!',
            icon: 'success',
            text: 'Thank you for loggin in to Zuitt Booking System',
          });

          fetch(
            'https://sheltered-badlands-71188.herokuapp.com/api/v1/user/details',
            {
              headers: {
                Authorization: `Bearer ${data.token}`,
              },
            }
          )
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data.isAdmin === true) {
                localStorage.setItem('email', data.email);
                localStorage.setItem('isAdmin', data.isAdmin);
                localStorage.setItem('_id', data._id);

                setUser({
                  email: data.email,
                  isAdmin: data.isAdmin,
                  _id: data._id,
                });
                history.push('/shop');
              } else {
                history.push('/');
              }
            });
        } else {
          Swal.fire({
            title: 'Oooops!',
            icon: 'error',
            text: 'Something Went Wrong. Check your Credentials',
          });
        }
        setEmail('');
        setPassword('');
      });
  }

  return (
    <Form onSubmit={(e) => login(e)}>
      <Form.Group controlId='userEmail'>
        <Form.Label>Email Address:</Form.Label>
        <Form.Control
          type='email'
          placeholder='Enter email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId='password'>
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type='password'
          placeholder='Enter Password'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>
      {loginButton ? (
        <Button variant='success' type='submit'>
          Submit
        </Button>
      ) : (
        <Button variant='success' type='submit' disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
