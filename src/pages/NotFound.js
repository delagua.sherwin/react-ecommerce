import React from 'react';
import { Link } from 'react-router-dom';
import { Modal, Button, Container } from 'react-bootstrap';

export default function Hello() {
  return (
    <Container>
      <Modal.Dialog>
        <Modal.Header closeButton>
          <Modal.Title>Error: 404</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <p>Page not Found</p>
        </Modal.Body>

        <Modal.Footer>
          <Button variant='primary' as={Link} to='/'>
            Go Home
          </Button>
        </Modal.Footer>
      </Modal.Dialog>
    </Container>
  );
}
