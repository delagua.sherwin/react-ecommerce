import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import IsAdmin from '../component/IsAdmin';

export default function Register() {
  const history = useHistory();
  const { user } = useContext(UserContext);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');

  const [registerButton, setRegisterButton] = useState(false);

  useEffect(() => {
    if (
      name !== '' &&
      email !== '' &&
      password !== '' &&
      verifyPassword !== '' &&
      password === verifyPassword
    ) {
      setRegisterButton(true);
    } else {
      setRegisterButton(false);
    }
  }, [name, email, password, verifyPassword]);

  function registerUser(e) {
    //let's describe the event that will happen upon registering a new user
    e.preventDefault(); //this is to avoid page redirection

    fetch('https://sheltered-badlands-71188.herokuapp.com/api/v1/user/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: name,
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        Swal.fire({
          title: 'Successful Registration',
          icon: 'success',
          text: 'Thank you for registering!',
        });

        history.push('/login');
      });

    setName('');
    setEmail('');
    setPassword('');
    setVerifyPassword('');
  }

  if (user.accessToken !== null) {
    return <Redirect to='/' />;
  }

  return (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId='name'>
        <Form.Label>First Name:</Form.Label>
        <Form.Control
          type='text'
          placeholder='Enter Name'
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId='userEmail'>
        <Form.Label>Email Address:</Form.Label>
        <Form.Control
          type='email'
          placeholder='Enter email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className='text-muted'>
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId='password1'>
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type='password'
          placeholder='Enter Password'
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId='password2'>
        <Form.Label>Verify Password:</Form.Label>
        <Form.Control
          type='password'
          placeholder='Verify Password'
          value={verifyPassword}
          onChange={(e) => setVerifyPassword(e.target.value)}
          required
        />

        <Form.Check
          type='switch'
          id='autoSizingCheck2'
          onChange={IsAdmin}
          label='Admin'
        />
      </Form.Group>
      {registerButton ? (
        <Button variant='primary' type='submit'>
          Submit
        </Button>
      ) : (
        <Button variant='primary' type='submit' disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
