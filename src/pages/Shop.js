import React, { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';
import Product from '../component/Product';

export default function Shop() {
  const [adminProduct, setAdminProduct] = useState([]);
  //for normal user
  const [allProduct, setAllProduct] = useState([]);

  const { user } = useContext(UserContext);
  console.log(user.accessToken);

  useEffect(() => {
    if (!user.isAdmin) {
      fetch(
        'https://sheltered-badlands-71188.herokuapp.com/api/v1/product/active',
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
        .then((response) => response.json())
        .then((userProduct) => {
          console.log(userProduct);
          setAllProduct(
            userProduct.map((enrollProduct) => {
              return (
                <Product
                  key={enrollProduct._id}
                  product={enrollProduct}
                  productId={enrollProduct._id}
                />
              );
            })
          );
        });
    } else {
      fetch(
        `https://sheltered-badlands-71188.herokuapp.com/api/v1/product/creator/${user._id}`,
        {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' },
        }
      )
        .then((res) => res.json())
        .then((activeProduct) => {
          console.log(activeProduct);
          setAdminProduct(
            activeProduct.map((product) => {
              return (
                <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td
                    className={
                      product.isActive ? 'text-success' : 'text-danger'
                    }
                  >
                    {product.isActive ? 'Active' : 'Inactive'}
                  </td>
                  {/* <td>
                    <DeleteButton courseId={course._id} />
                  </td>
                  <td>
                    <UpdateButton courseId={course._id} />
                  </td> */}
                </tr>
              );
            })
          );
        });
    }
  }, []);

  if (!user.isAdmin) {
    return [allProduct];
  } else {
    return (
      <>
        <Jumbotron className='mt-5'>
          <h1 className='text-center mb-5'>
            <strong>Course Dashboard</strong>
          </h1>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>{adminProduct}</tbody>
          </Table>
        </Jumbotron>
      </>
    );
  }
}
