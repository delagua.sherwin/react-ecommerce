import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';

export default function SingleCocktail() {
  const { productId } = useParams();
  const [product, setProduct] = useState([]);
  console.log(useParams());

  useEffect(() => {
    async function getProduct() {
      try {
        const response = await fetch(
          `https://sheltered-badlands-71188.herokuapp.com/api/v1/product/${productId}`
        );
        const data = await response.json();
        console.log(data);
        if (data) {
          setProduct(data);
        } else {
          setProduct([]);
        }
      } catch (error) {
        console.log(error);
      }
    }
    getProduct();
  }, [productId]);

  if (!product) {
    return <h2 className=''>no cocktail to display</h2>;
  } else {
    const { name, image, description, brand, price, countInStock } = product;
    return (
      <section className=''>
        <Link to='/shop' className='btn btn-primary'>
          back home
        </Link>
        <h2 className=''>{name}</h2>
        <div className=''>
          <img src={image} alt={name}></img>
          <div className=''>
            <p>
              <span className=''>name :</span> {name}
            </p>
            <p>
              <span className=''>brand :</span> {brand}
            </p>
            <p>
              <span className=''>Description :</span> {description}
            </p>
            <p>
              <span className=''>Price :</span> {price}
            </p>
            <p>
              <span className=''>Stock :</span> {countInStock}
            </p>
          </div>
        </div>
      </section>
    );
  }
}
